#!/bin/sh

curl https://geodatamine.fr/data/bicycle_parking/-9205005\?format\=geojson\&aspoint\=true -o stationnements_cyclables_limoges.geojson
cat stationnements_cyclables_limoges.geojson | jq -c -f extract_limoges_metropole_parking.jq > stationnements_limoges_metropole.geojson
#rm stationnements_cyclables_limoges.geojson
