#!/bin/sh

sed -i 's/87005:/Aureil:/' $1
sed -i 's/87019:/Boiseuil:/' $1
sed -i 's/87020:/Bonnac-la-Côte:/' $1
sed -i 's/87038:/Chaptelat:/' $1
sed -i 's/87048:/Condat-sur-Vienne:/' $1
sed -i 's/87050:/Couzeix:/' $1
sed -i 's/87063:/Eyjeaux:/' $1
sed -i 's/87065:/Feytiat:/' $1
sed -i 's/87075:/Isle:/' $1
sed -i 's/87085:/Limoges:/' $1
sed -i 's/87113:/Le Palais-sur-Vienne:/' $1
sed -i 's/87114:/Panazol:/' $1
sed -i 's/87118:/Peyrilhac:/' $1
sed -i 's/87125:/Rilhac-Rancon:/' $1
sed -i 's/87143:/Saint-Gence:/' $1
sed -i 's/87156:/Saint-Just-le-Martel:/' $1
sed -i 's/87192:/Solignac:/' $1
sed -i 's/87201:/Verneuil-sur-Vienne:/' $1
sed -i 's/87202:/Veyrac:/' $1
sed -i 's/87205:/Le Vigen:/' $1
