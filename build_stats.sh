#!/bin/sh

cat $1 | jq -r -f limoges_metropole_capacity.jq > stats_stationnements.txt
echo "La répartition est la suivante :" >> stats_stationnements.txt
cat $1 | jq -r -f capacity_per_city.jq > stats_per_city.txt
./replace_insee_code_by_city_names.sh stats_per_city.txt
cat stats_per_city.txt >> stats_stationnements.txt
rm stats_per_city.txt
