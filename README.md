# Stationnements vélo Limoges OSM

Quelques scripts pour manipuler les données Openstreetmap (au format geojson) pour un affichage sur umap et quelques statistiques.

## Composition du repo

* build_stats.sh : script linux générant un fichier contenant quelques statistiques sur les stationnements cyclables gérés par Limoges Métropole.
Ce script prend en argument le fichier geojson contenant les données issues d'Openstreetmap téléchargées depuis [geodatamine](geodatamine.fr/).
Ex : `./build_stats.sh stationnements.geojson`

* capacity_per_city.jq : script jq permettant de sortir le nombre de stationnements par commune (par numéro insee).
L'utilisation se fait avec la commande suivante : `cat geodatamine_file.geojson | jq -r -f capacity_per_city.jq`

* download_bicycle_parkings_and_extract_lim_metro.sh : script linux téléchargeant le fichier geojson contenant les stationnements cyclables sur le territoire de Limoges Métropole (stationnements_cyclables_limoges.geojson) et extrayant les stationnements gérés par Limoges Métropole (stationnements_limoges_metropole.geojson).

* extract_limoges_metropole_parking.jq : script jq permettant d'extraire d'un fichier geojson les stationnements cyclables gérés par Limoges Métropole.
Utilisation : `cat geodatamine_file.geojson | jq -r -f extract_limoges_metropole_parking.jq`

* limoges_metropole_capacity.jq : script jq permettant d'extraire le nombre de points de stationnements et leur capacité sur Limoges Métropole.
Utilisation : `cat geodatamine_file.geojson | jq -r -f limoges_metropole_capacity.jq`
Sortie : Il existe xx points de stationnements cyclable comprenant yy places répertoriées dans Limoges Métropole.

* replace_insee_code_by_city_names.sh : Script linux permettant de remplacer les codes insee par le nom des communes dans le fichier de sortie du script capacity_per_city.jq

## Pipeline

Un pipeline tourne chaque vendredi à 5h.
Une machine virtuelle ubuntu est lancée et exécute un script téléchargeant les données cyclables provenant d'openstreetmap, filtrant les stationnements associés à Limoges Métropole et calcule quelques statistiques sur ces données.

Le résultat de ces pipelines est disponible à l'adresse suivante : [https://gitlab.com/F1oron/stationnements-velo-limoges-osm/-/jobs](https://gitlab.com/F1oron/stationnements-velo-limoges-osm/-/jobs).
Un bouton à droite de chaque itération permet de télécharger les fichiers générer lors du déroulement du pipeline.
![](assets/screenshot_pipeline.png)

Les fichiers générés sont les suivants :

1. stationnements_cyclables_limoges.geojson : fichier téléchargé depuis [geodatamine](https://geodatamine.fr/) avec les paramètres suivant :
	* Thématique : Parking à vélos
	* Territoire : Limoges Métropole
	* Format de fichier : Geojson
	* Tout siplifier sous forme de points : [x]
	* Ajouter des métadonnées : [ ]
	* Rayon de recherche : Emprise de la zone sélectionnée (minimum)
2. stationnements_limoges_metropole.geojson : copie du fichier précédent, en ne gardant que les stationnements gérés par Limoges Métropole
3. stats_stationnements.txt : Statistiques sur le nombre de stationnements gérés par Limoges Métropole et leur répartition par commune.

## Utilisation des données sur une carte umap

Les fichiers geojson sont utilisables comme sources de données sur une carte [umap](https://umap.openstreetmap.fr).
Pour cela, se rendre sur le site, se connecter (un compte Openstreetmap est nécessaire) et créer une nouvelle carte.

Activer l'édition (bouton en haut à droite de l'écran) puis bouton Importer des données.

Sélection le fichier à importer, le calque, puis bouton Importer.
Les stationnements doivent apparaitre sur la carte.

Ensuite, configurer les informations apparaissants dans les popup lors d'un clic sur un stationnement.
Aller dans le menu Gérer les calques, éditer le calque où les données ont été importées, Options d'interaction > Gabarit du contenu de la popup
Une aide est disponible pour le formatage du texte (gras, italique, image, ...), pour utiliser des données depuis le fichier geojson, utiliser un attribut entre parenthèses (ex : {nom}).
Les attributs disponibles dans le geojson sont :
* code_com : numéro INSEE de la commune
* capacite : nombre de stationnements disponibles sur ce mobilier
* capacite_cargo : nombre de stationnements vélos cargo disponibles sur ce mobilier
* type_accroche : valeurs posibles :
	* ROUE
	* CADRE
	* CADRE ET ROUE
	* SANS ACCROCHE
* mobilier : valeurs possibles
	* ARCEAU
	* RATELIER
	* RACK DOUBLE ETAGE
	* CROCHET
	* SUPPORT GUIDON
	* POTELET
	* ARCEAU VELO GRANDE TAILLE
	* AUCUN EQUIPEMENT
	* AUTRE
* acces : valeurs possibles
	* LIBRE ACCES
	* ABONNEMENT OU INSCRIPTION PREALABLE
	* PRIVE
* gratuit : gratuité du stationnement (valeur : true / false)
* protection : valeurs possibles
	* STATIONNEMENT NON FERME
	* CONSIGNE COLLECTIVE FERMEE
	* BOX INDIVIDUEL FERME
	* AUTRE
* couverture : Indique la protection contre les précipitations (true / false)
* surveillance : Indique si le stationnement est protégé par surveillance ou vidéo-surveillance (true / false)
* lumiere : Indique si le stationnement est éclairé (true / false)
* proprietaire : Propriétaire de l'équipement
* gestionnaire : Gestionnaire de l'équipement

Un exemple de carte avec les options d'interraction ci-dessous est disponible à l'adresse suivante : [http://u.osmfr.org/m/491893/](http://u.osmfr.org/m/491893/)
```
**Type :** {mobilier}
**Protection :** {protection}
**Capacité :** {capacite}
```
